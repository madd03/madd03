### Hi there 👋

I create content for [Source Engine](https://developer.valvesoftware.com/wiki/Source) games (mainly Garry's Mod and Half-Life 2) like scripts, plugins and maps. 

I’m currently learning [Lua](https://www.lua.org/about.html) and [Hammer Editor](https://developer.valvesoftware.com/wiki/Valve_Hammer_Editor)

My current personal project is: [Garry's Mod Bhop](https://gitlab.com/madd03/gmod-bhop)
